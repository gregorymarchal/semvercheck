# semvercheck

Validate SemVer version string.

It isn't perfect but good enough to suits some needs.


## Language(s)

POSIX shell.


## Usage

It can be used stand-alone like this:

    semvercheck "version_string"

... or it can be embedded in scripts like this:

    semvercheck "${_version_string}" >/dev/null 2>&1

It exits 0 if the version string is valid and exits 1 if the string is invalid.
